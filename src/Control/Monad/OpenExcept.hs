module Control.Monad.OpenExcept where

import Control.Monad.Except
import Data.OpenUnion
import Data.Typeable
import GHC.Exts

-- | First argument (how to eliminate?) is the actual elements in
-- union, the third - is the elements which must be contained inside
-- of it.
type family CanThrow (els :: [*]) (m :: * -> *) (throws :: [*]) :: Constraint where
  CanThrow els m '[] = (MonadError (Union els) m)
  CanThrow els m (err ': errs) =
    ( Typeable err
    , '[err] :< els
    , CanThrow els m errs )

openThrow
  :: forall els e m a
   . (MonadError (Union els) m, Typeable e, '[e] :< els)
  => e
  -> m a
openThrow e
  = throwError
  $ (liftUnion e :: Union els)
