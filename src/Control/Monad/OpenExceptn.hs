module Control.Monad.OpenExceptn where

import Control.Monad.Except
import Data.OpenUnion


type OpenError e m =
  ( MonadError (Union els) m
  , e :< els )

openThrow
  :: (OpenError e m)
  => e
  -> m a
openThrow e = throwError $ liftUnion e
