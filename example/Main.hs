module Main where

import Control.Monad.Except
import Control.Monad.OpenExcept
import Data.OpenUnion

data Error1 = Error1
  deriving (Show)

data Error2 = Error2
  deriving (Show)

data Error3 = Error3
  deriving (Show)

type EList1 = '[Error1, Error2]

type EList2 = '[Error2, Error3]

type EList3 = '[Error1, Error2, Error3]

action1 :: (CanThrow els m EList1) => m ()
action1 = do
  openThrow Error3
  -- openThrow Error1
  -- openThrow Error2

-- action2 :: (CanThrow EList2 m) => m ()
-- action2 = openThrow Error3

-- action12 :: (CanThrow EList3 m) => m ()
-- action12 = do
--   action1
--   action2

main :: IO ()
main = do
  e :: Either (Union EList1) ()
    <- runExceptT $ action1
  case e of
    Left x  -> print $ show1 x
    Right _ -> print "right"
  return ()


show1 :: Union EList1 -> String
show1 = (show :: Error1 -> String) @>
        (show :: Error2 -> String) @>
        typesExhausted
